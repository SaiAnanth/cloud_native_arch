package lru

import (
	"errors"
)

// Cacher implements gets and puts methods
type Cacher interface {
	Get(interface{}) (interface{}, error)
	Put(interface{}, interface{}) error
}

type lruCache struct {
	size      int
	remaining int
	cache     map[string]string
	queue     []string
}

// NewCache Create a new New Cache
func NewCache(size int) Cacher {
	return &lruCache{size: size, remaining: size, cache: make(map[string]string), queue: make([]string, 0)}
}

func (lru *lruCache) Get(key interface{}) (interface{}, error) {
	// check the key type
	keystr, ok := key.(string)
	if !ok {
		return nil, errors.New("invalid Key")
	}

	// check if it is in the map
	val, ok := lru.cache[keystr]
	if !ok {
		return nil, errors.New("key not found")
	}

	// remove from the queue and put it in the end
	lru.qDel(keystr)

	lru.queue = append(lru.queue, keystr)

	return val, nil
}

func (lru *lruCache) Put(key, val interface{}) error {
	// ckeck the key type
	keystr, ok := key.(string)
	if !ok {
		return errors.New("invalid Key")
	}
	// check value type
	valstr, ok := val.(string)
	if !ok {
		return errors.New("invalid Value")
	}

	// if queue is full remove the head element
	if lru.remaining == 0 {
		delete(lru.cache, lru.queue[0])
		lru.qDel(lru.queue[0])
		lru.remaining++
	}
	// inset the new key at the end
	lru.cache[keystr] = valstr
	lru.queue = append(lru.queue, keystr)
	lru.remaining--
	return nil
}

// Delete element from queue
func (lru *lruCache) qDel(ele string) {
	for i := 0; i < len(lru.queue); i++ {
		if lru.queue[i] == ele {
			oldlen := len(lru.queue)
			copy(lru.queue[i:], lru.queue[i+1:])
			lru.queue = lru.queue[:oldlen-1]
			break
		}
	}
}
