package main

import (
	"fmt"
	"labs/lab2/lru"
	"strconv"
)

func main() {
	testlru := lru.NewCache(3)

	for i := 0; i < 4; i++ {
		err := testlru.Put("key"+strconv.Itoa(i), "val"+strconv.Itoa(i))
		if err != nil {
			fmt.Println(err)
		}
	}
	if val, err := testlru.Get("key0"); err != nil {
		fmt.Println(err)
	} else if val.(string) != "val0" {
		fmt.Println(err)
	}
}
