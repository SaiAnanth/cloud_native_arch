// Package main implements a server for movieinfo service.
package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"time"

	"grpc/movieapi"

	"google.golang.org/grpc"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	port            = ":50051"
	mongodbEndpoint = "mongo://mongo:27017" // 172.18.0.2
)

// server is used to implement movieapi.MovieInfoServer
type server struct {
	movieapi.UnimplementedMovieInfoServer
	client *mongo.Client
}

type movie struct {
	Name     string   `bson:"name"`
	Year     int32    `bson:"year"`
	Director string   `bson:"director"`
	Cast     []string `bson:"cast"`
}

// Map representing a database
// var moviedb = map[string][]string{"Pulp fiction": {"1994", "Quentin Tarantino", "John Travolta,Samuel Jackson,Uma Thurman,Bruce Willis"}}

// GetMovieInfo implements movieapi.MovieInfoServer
func (s *server) GetMovieInfo(ctx context.Context, in *movieapi.MovieRequest) (*movieapi.MovieReply, error) {

	title := in.GetTitle()
	log.Printf("Received: %v", title)

	movieCollection := s.client.Database("movie").Collection("movies")

	m := &movie{}
	filter := bson.M{"name": bson.M{"$eq": title}}
	err := movieCollection.FindOne(ctx, filter).Decode(m)
	checkError(err)

	reply := &movieapi.MovieReply{
		Year:     m.Year,
		Director: m.Director,
		Cast:     m.Cast,
	}

	return reply, nil
}

// SetMovieInfo sets the given movie info in the Database
func (s *server) SetMovieInfo(ctx context.Context, in *movieapi.MovieData) (*movieapi.Status, error) {
	reply := &movieapi.Status{}

	movieCollection := s.client.Database("movie").Collection("movies")

	filter := bson.M{"name": bson.M{"$eq": in.GetTitle()}}

	result := movieCollection.FindOne(ctx, filter).Decode(&movie{})
	if result == nil {
		reply.Code = "Already Exists"
	} else {
		_, err := movieCollection.InsertOne(ctx, &movie{
			Name:     in.GetTitle(),
			Director: in.GetDirector(),
			Year:     in.GetYear(),
			Cast:     in.GetCast(),
		})
		checkError(err)
		reply.Code = "Insert Successful"
	}

	return reply, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	server := &server{}

	server.client, err = mongo.NewClient(
		options.Client().ApplyURI(mongodbEndpoint),
	)

	checkError(err)

	fmt.Println("server Started")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = server.client.Connect(ctx)
	checkError(err)

	defer server.client.Disconnect(ctx)
	movieapi.RegisterMovieInfoServer(s, server)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
