module labs

go 1.15

require (
	github.com/go-openapi/errors v0.20.0
	github.com/go-openapi/loads v0.20.2
	github.com/go-openapi/runtime v0.19.26
	github.com/go-openapi/spec v0.20.3
	github.com/go-openapi/strfmt v0.20.0
	github.com/go-openapi/swag v0.19.14
	github.com/golang/protobuf v1.4.3
	github.com/jessevdk/go-flags v1.4.0
	go.mongodb.org/mongo-driver v1.4.6
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.25.0
)
