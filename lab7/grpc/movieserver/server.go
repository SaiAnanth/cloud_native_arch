// Package main implements a server for movieinfo service.
package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"

	"grpc/movieapi"

	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

// server is used to implement movieapi.MovieInfoServer
type server struct {
	movieapi.UnimplementedMovieInfoServer
}

// Map representing a database
var moviedb = map[string][]string{"Pulp fiction": {"1994", "Quentin Tarantino", "John Travolta,Samuel Jackson,Uma Thurman,Bruce Willis"}}

// GetMovieInfo implements movieapi.MovieInfoServer
func (s *server) GetMovieInfo(ctx context.Context, in *movieapi.MovieRequest) (*movieapi.MovieReply, error) {
	title := in.GetTitle()
	log.Printf("Received: %v", title)
	reply := &movieapi.MovieReply{}
	val, ok := moviedb[title]
	if !ok { // Title not present in database
		return reply, nil
	}

	if year, err := strconv.Atoi(val[0]); err != nil {
		reply.Year = -1
	} else {
		reply.Year = int32(year)
	}
	reply.Director = val[1]
	cast := strings.Split(val[2], ",")
	reply.Cast = append(reply.Cast, cast...)

	return reply, nil

}

// SetMovieInfo sets the given movie info in the Database
func (s *server) SetMovieInfo(ctx context.Context, in *movieapi.MovieData) (*movieapi.Status, error) {
	reply := &movieapi.Status{}

	if _, ok := moviedb[in.GetTitle()]; ok {
		reply.Code = "Already Exists"
	} else {
		moviedb[in.GetTitle()] = []string{
			strconv.Itoa(int(in.GetYear())),
			in.GetDirector(),
			strings.Join(in.GetCast(), ","),
		}
		reply.Code = "Success"
		log.Printf("Added Movie: %v", in)
	}

	return reply, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	fmt.Println("server Started")
	movieapi.RegisterMovieInfoServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
