// Package main imlements a client for movieinfo service
package main

import (
	"context"
	"log"
	"os"
	"time"

	"labs/lab5/movieapi"

	"google.golang.org/grpc"
)

const (
	address      = "localhost:50051"
	defaultTitle = "Pulp fiction"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := movieapi.NewMovieInfoClient(conn)

	// Contact the server and print out its response.
	title := defaultTitle
	if len(os.Args) > 1 {
		title = os.Args[1]
	}
	// Timeout if server doesn't respond

	newMovie := &movieapi.MovieData{
		Title:    "Tenet",
		Year:     2020,
		Director: "Christopher Nolan",
		Cast:     []string{"Robert Pattinson", "John David Washington"},
	}

	addMovie(client, newMovie)

	getMovie(client, title)
}

func addMovie(client movieapi.MovieInfoClient, movie *movieapi.MovieData) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	resp, err := client.SetMovieInfo(ctx, movie)
	if err != nil {
		log.Fatalf("could not get movie info: %v", err)
	}
	log.Printf("Movie %s :%s", movie.Title, resp.Code)

}

func getMovie(client movieapi.MovieInfoClient, title string) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	resp, err := client.GetMovieInfo(ctx, &movieapi.MovieRequest{Title: title})
	if err != nil {
		log.Fatalf("could not get movie info: %v", err)
	}
	log.Printf("Movie Info for %s %d %s %v", title, resp.GetYear(), resp.GetDirector(), resp.GetCast())
}
